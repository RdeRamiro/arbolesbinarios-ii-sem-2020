/**
 * ---------------------------------------------------------------------
 * $Id: ArbolBinario.java,v 2.0 2020/12/14 
 * Universidad Francisco de Paula Santander 
 * Programa Ingenieria de Sistemas
 * Estructuras de datos
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package ufps.util.colecciones_seed;
import java.util.Iterator;

/**
 * Implementacion de Clase para el manejo de un Arbol Binario.
 * @param <T> Tipo de datos a almacenar en el Arbol Binario.
 * @author Marco Adarme
 * @version 2.0
 */
public class ArbolBinario<T>
{
    
    ////////////////////////////////////////////////////////////
    // ArbolBinario - Atributos ////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Nodo raiz del Arbol Binario
     */
    private NodoBin<T> raiz;        
    
    

    ////////////////////////////////////////////////////////////
    // ArbolBinario - Implementacion de Metodos ////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Crea un Arbol Binario vacio. <br>
     * <b>post: </b> Se creo un Arbol Binario vacio.<br>
     */
     public ArbolBinario(){
        this.raiz=null;
     }
     
    /**
     * Crea un Arbol Binario con una raiz predefinida. <br>
     * <b>post: </b> Se creo un nuevo Arbol con su raiz definida.<br>
     * @param raiz  Un objeto de tipo T que representa del dato en la raiz del Arbol. <br>
     */
     public ArbolBinario(T raiz) {
        this.raiz = new NodoBin(raiz);
     }
     
    /**
     * Metodo que permite conocer el objeto de la raiz del Arbol Binario. <br>
     * <b>post: </b> Se obtuvo la raiz del Arbol Binario.<br>
     * @return la raiz del Arbol Binario.
     */
    public T getObjRaiz() {
        return (raiz.getInfo());
    }
     
    /**
     * Metodo que permite conocer la raiz del Arbol Binario. <br>
     * <b>post: </b> Se obtuvo la raiz del Arbol Binario.<br>
     * @return la raiz del Arbol Binario.
     */
    public NodoBin<T> getRaiz() {
        return raiz;
    }
    
    /**
     * Metodo que permite modificar la raiz del Arbol Binario. <br>
     * <b>post: </b> Se modifico la raiz del Arbol Binario.<br>
     * @param raiz representa la nueva raiz del Arbol Binario.
     */
    public void setRaiz(NodoBin<T> raiz) {
        this.raiz = raiz;
    }
        
   
    /**
     * Metodo que permite mostrar por consola la informacion del Arbol Binario. <br>
     * @param n Representa la raiz del ArbolBinario o de alguno de sus subarboles.
     */
    public void imprime(NodoBin<T> n) {
        T l = null;
        T r = null;
        if(n==null)
            return;
        if(n.getIzq()!=null) {
         l = n.getIzq().getInfo();
        }
        if(n.getDer()!=null) {
         r =n.getDer().getInfo();
        }       
        System.out.println("NodoIzq: "+l+"\t Info: "+n.getInfo()+"\t NodoDer: "+r);
        if(n.getIzq()!=null) {
         imprime(n.getIzq());
        }
        if(n.getDer()!=null) {
         imprime(n.getDer());
        }
    }
    
   
   
}//Fin de la Clase ArbolBinario
